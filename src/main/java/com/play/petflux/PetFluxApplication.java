package com.play.petflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetFluxApplication {

	public static void main(String[] args) {


		SpringApplication.run(PetFluxApplication.class, args);
//		GreetingWebClient gwc = new GreetingWebClient();
//		System.out.println(gwc.getResult());
	}

}
