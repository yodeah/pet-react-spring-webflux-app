package com.play.petflux.authorization;

import org.springframework.http.HttpStatus;

public class GenericResponse {
    final String message;

    public GenericResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
