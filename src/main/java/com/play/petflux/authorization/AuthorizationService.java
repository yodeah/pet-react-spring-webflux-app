package com.play.petflux.authorization;

import com.play.petflux.authorization.security.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthorizationService {

    @Autowired
    UserRepository userRepository;

    public void createNewUser(String email, String password) throws Exception {
        if (userRepository.doesUserExist(email)) {
            throw new Exception("User with email has been already created");
        } else {
            userRepository.createUser(email, password);
        }
    }

    public UserDetails getUserDetails(String email) throws Exception {
        Optional<UserEntity> user = userRepository.getUserByEmail(email);

        if(user.isPresent()){
            return new UserDetails(user.get().email,user.get().password);
        }else{
            throw new Exception("User doesnt exist");
        }
    }
}
