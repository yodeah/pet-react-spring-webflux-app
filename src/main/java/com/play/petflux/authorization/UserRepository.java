package com.play.petflux.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    @Autowired
    JdbcTemplate psqlDataSource;

    public List<UserEntity> getUsers() {
        final String sql = "SELECT id, email, password FROM users";
        return psqlDataSource.query(sql, new BeanPropertyRowMapper<UserEntity>(UserEntity.class));
    }

    public Optional<UserEntity> getUserByEmail(String email) {
        //TODO use prepared statement, get 0 is ugly
        try {
            final String sql = String.format("SELECT id, email, password FROM users WHERE email='%s' LIMIT 1", email);
            List<UserEntity> result = psqlDataSource.query(sql,
                    new BeanPropertyRowMapper<UserEntity>(UserEntity.class));

            if (result.size() == 0) {
                return Optional.empty();
            } else {
                return Optional.of(result.get(0));
            }
        } catch (DataAccessException e) {
            System.out.println("user doesnt exist");
            return Optional.empty();
        }
    }

    public boolean doesUserExist(String email) {
        return getUserByEmail(email).isPresent();
    }

    public void createUser(String email, String password) throws Exception {
        Integer numberOfRowsAffected = psqlDataSource.update("INSERT INTO public.users( email,  password) VALUES (?, " +
                "?);", email, password);
        if (numberOfRowsAffected == 0) {
            throw new Exception("User wasnt created");
        }
    }
}
