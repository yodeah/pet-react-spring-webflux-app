package com.play.petflux.authorization.security;

import com.play.petflux.authorization.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationManger implements ReactiveAuthenticationManager {

    @Autowired
    AuthorizationService authorizationService;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) throws AuthenticationException {
        try {
            UserDetails user = authorizationService.getUserDetails(authentication.getPrincipal().toString());
            if(user.password.equals(authentication.getCredentials().toString())){
               return Mono.just(new AuthenticationImpl(true, user));
            }
            throw new AuthException("Invalid password");
        } catch (Exception e) {
           throw new AuthException(e.getMessage());
        }
    }
}
