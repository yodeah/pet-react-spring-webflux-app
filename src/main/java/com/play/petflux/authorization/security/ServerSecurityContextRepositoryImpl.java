package com.play.petflux.authorization.security;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;

@Repository
public class ServerSecurityContextRepositoryImpl implements ServerSecurityContextRepository {

    HashMap<ServerWebExchange, SecurityContext> exchanges = new HashMap();

    @Override
    public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
//        if(exchanges.containsKey(exchange)){
//            exchanges.
//        }
        exchanges.put(exchange,context);
        return Mono.empty();
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange exchange) {
        if(exchanges.containsKey(exchange)){
            return Mono.just(exchanges.get(exchange));
        } else {
            return Mono.just(new SecurityContextImpl(new AuthenticationImpl(false, null)));
        }
    }
}
