package com.play.petflux.authorization;

import com.play.petflux.authorization.security.JwtResponse;
import com.play.petflux.authorization.security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class AuthorizationHandler {

    @Autowired
    AuthorizationService authorizationService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private ReactiveAuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    public Mono<ServerResponse> authenticate(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(RegistrationHttpRequest.class)
                .flatMap(r -> {
                    try {
                        authenticate(r.getEmail(), r.getPassword());


                        final UserDetails userDetails = authorizationService.getUserDetails(r.getEmail());
                        System.out.println("hello");
                        final String token = jwtTokenUtil.generateToken(userDetails);

                        return ServerResponse.ok()
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(Mono.just(new JwtResponse(token)), JwtResponse.class);
                    } catch (Exception e) {
                        return ServerResponse.status(HttpStatus.NOT_ACCEPTABLE)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(Mono.just(new GenericResponse(e.getMessage())), GenericResponse.class);
                    }
                });
    }


    private void authenticate(String email, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public Mono<ServerResponse> register(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(AuthenticationHttpRequest.class)
                .flatMap(r -> {
                    try {
                        authorizationService.createNewUser(r.email, r.password);
                        return ServerResponse.ok()
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(Mono.just(new GenericResponse("User created")), GenericResponse.class);
                    } catch (Exception e) {
                        return ServerResponse.status(HttpStatus.NOT_ACCEPTABLE)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(Mono.just(new GenericResponse(e.getMessage())), GenericResponse.class);
                    }
                });
    }

    public Mono<ServerResponse> listUsers(ServerRequest request) {
        List<UserEntity> users = userRepository.getUsers();
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(Mono.just(users), List.class);
    }

    public Mono<ServerResponse> hello(ServerRequest request) {
        System.out.println("hello request");
        return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                .body(BodyInserters.fromObject("Hello, Spring!"));
    }
}
