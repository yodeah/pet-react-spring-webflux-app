package com.play.petflux;

import com.play.petflux.authorization.AuthorizationHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class Router {

    @Bean
    public RouterFunction<ServerResponse> route(AuthorizationHandler authorizationHandler) {
        return RouterFunctions
                .route(RequestPredicates.GET("/hello")
                        .and(RequestPredicates.accept(MediaType.TEXT_PLAIN)), authorizationHandler::hello)
                .andRoute(RequestPredicates.GET("/users")
                        .and(RequestPredicates.accept(MediaType.TEXT_PLAIN)), authorizationHandler::listUsers)
                .andRoute(RequestPredicates.POST("/authorization/register")
                        .and(RequestPredicates.accept(MediaType.APPLICATION_JSON)), authorizationHandler::register)
                .andRoute(RequestPredicates.POST("/authorization/authenticate")
                        .and(RequestPredicates.accept(MediaType.APPLICATION_JSON)), authorizationHandler::authenticate);
    }
}
