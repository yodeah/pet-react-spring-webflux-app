package com.play.petflux;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@ComponentScan("com.play.petflux")
public class SpringJdbcConfig {
    @Bean
    public DataSource psqlDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/docker");
        dataSource.setUsername("docker");
        dataSource.setPassword("mysecretpassword");
//        dataSource.setDriverClassName("spring.datasource.driver");
//        dataSource.setUrl("spring.datasource.url");
//        dataSource.setUsername("spring.datasource.username");
//        dataSource.setPassword("spring.datasource.password");

        return dataSource;
    }
}
