CREATE TABLE public.users
(
    email varchar,
    id SERIAL NOT NULL,
    password varchar,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.users
    OWNER to docker;




INSERT INTO public.users( email,  password) VALUES ('hello@hello.com', 'hashedpw');