import { WishListItem, WishList } from "./WishList";
import { isType, getSnapshot } from "mobx-state-tree";

it("can create a model", () => {
    const item = WishListItem.create({
        "name" : "Narnia",
        "price": 28.67,
        "image": "https://via.placeholder.com/150"
    })

    expect(item.price).toBe(28.67)


    const item2 = WishListItem.create({
        "name" : "Narnia",
        "price": 28.67
    })

    expect(item2.price).toBe(28.67)
    expect(item2.image).toBe("")
})

it("can create a WishList", () => {
    const list = WishList.create({
        items: [
            {
                name: "Narnia Chronicles",
                price: 55
            }
        ]
    })

    expect(list.items.length).toBe(1)
    expect(list.items[0].price).toBe(55)
})


it("can push an item to a WishList", () => {
    const list = WishList.create({
        items: []
    })

    list.addItem({name:"nn",price:22})

    expect(list.items.length).toBe(1)
    expect(list.items[0].price).toBe(22)
})

it("can modify an item in a WishList", () => {
    const list = WishList.create({
        items: []
    })

    list.addItem({name:"nn",price:22})
    list.items[0].changePrice(67)

    expect(list.items.length).toBe(1)
    expect(list.items[0].price).toBe(67)
})

it("will generate a snapshot equal to the element added", () => {
    const list = WishList.create({
        items: []
    })

    list.addItem({name:"nn",price:22})
    list.items[0].changeImage("hello")

    expect(getSnapshot(list)).toMatchSnapshot({items:[{name:"nn",price:22,image:"hello"}]});
})

it("will sum the price of the items in the list", () => {
    const list = WishList.create({
        items: []
    })

    list.addItem({name:"book1",price:22})
    list.addItem({name:"book2",price:23})

    expect(list.totalPrice).toBe(45);
})