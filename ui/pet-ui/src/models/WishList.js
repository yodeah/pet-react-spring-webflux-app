import {types} from "mobx-state-tree"

const data = {
    "name" : "Narnia",
    "price": 28.67,
    "image": "https://via.placeholder.com/150"
}

export const WishListItem = types
.model({
    name: types.string,
    price: types.number,
    image: ""
})
.actions(self => ({
    changeName(newName) {
        self.name = newName;
    },
    changePrice(newPrice){
        self.price = newPrice
    },
    changeImage(newImage){
        self.image = newImage
    }
}))

export const WishList = types
.model({
    items: types.optional(types.array(WishListItem), [])
})
.actions(self => ({
    addItem(newItem) {
        self.items.push(newItem)
    }
}))
.views(self => ({
    get totalPrice() {
        return self.items.reduce((sum, entry) => sum + entry.price, 0)
    }
}))
